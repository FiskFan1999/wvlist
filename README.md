# Note: frozen.
On 25 July, 2023, wvlist.net was closed. Development on this project has been stopped and this repository will now be archived.

# wvlist
WVList is a Werke Verzeichnis database available over HTTP(s) with community submissions. It is being hosted by the creator at [wvlist.net](https://wvlist.net) [(onion service)](http://xtjuwqe4hlqojbknh2kwlm5nh4usj2yh5ceuh2x4gy27wirurxh3qoid.onion/), and can be hosted by anyone else as well. This project is intended to be a reference tool for the catalogue complete works list of composers and the numbering list used to identify each composition of a composer, even when ambiguous titles are used. Well-known examples of WV catalogues are [Bach Werke Verzeichnis](https://www.bach-cantatas.com/Bach-Werke-Verzeichnis.pdf), [K numbers used to classify Mozart compositions](https://www.mozartproject.org/what-are-mozart-k-numbers/), and using Opus numbers to classify published compositions.

# Dependencies
In addition to the modules imported by the Go compiler, this project requires:
- Lilypond music engraving software version >=2.20.0. This can be installed in many ways. Instructions for how to install Lilypond can be found [here](https://lilypond.org/download.html). 
- jQuery >=3.5.1 (this is fulfilled via a HTTP request, by default to googleapis).

# Installation
- Install Lilypond.
  - Note: the user is advised to avoid mainstream package repositories and instead install Lilypond in one of the ways mentioned [here](https://lilypond.org/download.html)
  - To install the latest stable build of lilypond to linux operating systems, follow [these instructions](https://lilypond.org/unix.html)
- Clone the repository into your VPS, and change directory into the home directory of this project.

`git clone https://github.com/FiskFan1999/wvlist.git && cd wvlist`
- Highly recommended: checkout to the latest release instead of unstable HEAD
- Copy the default configuration file.

`cp config.default.json config.json`
- Change the configuration settings as required. (Refer to `config.txt` to describe each )
- Compile the code via `make`
- Run wvlist via `./wvlist run`. Refer to the following instructions about flags to use.

# Syntax

```bash
./wvlist [-c /path/to/config.json] [-p port] [-t TLS-port] [-d] [-k /path/to/chain.pem] [-x /path/to/secret.pem] run
./wvlist sendemail <address>
./wvlist password [passwd]
```

# Runtime flags
| name | default | definition
--- | --- | ---
| -c | "./config.json" | Path to configuration file. The user should make a copy of `/config.default.json` and fill in the appropriate information. Will panic at runtime if the file is not found.
| -p | 6060 | Port to listen to on plaintext. Set to `0` to disable.
| -t | 0 | Port to listen to on TLS (see -k and -x). Set to `0` to disable.
| -d | 0 | Debug mode. TLS listener listens over plaintext (useful for testing administrator console). This should (almost never) be used*.
| -k | "" | Path to full chain of TLS certification (such as that which is generated by letsencrypt)
| -x | "" | Path to secret file of TLS certification (such as that which is generated by letsencrypt)

* If the administrator intends to run wvlist through a reverse proxy using an application such as nginx, the following command is suggested (note that requiring the use of wvlist with a reverse proxy is planned for a future major update):

```bash
PORT=12345 ./wvlist -d -p 0 -t $PORT run
```

Note: Since v1.3.0 this workaround is not required anymore, and the administrator can simply run.

```bash
PORT=12345 ./wvlist -p $PORT run
```

# Chat
Please feel free to join the conversation on IRC. (click to open browser client)

Main networks:
- [`irc.ergo.chat:6697 (TLS) #wvlist`](https://ergo.chat/kiwi/#wvlist)
- [`irc.libera.chat#wvlist`](https://web.libera.chat/#wvlist) - Also available on Matrix at `#wvlist:libera.chat`
