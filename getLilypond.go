package main

import (
	"bytes"
	"encoding/csv"
	"errors"
	"fmt"
	"html"
	"io/fs"
	"log"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	LilypondDirPath = "./lilypond/"
)

type LilypondFileToMakeStr struct {
	Command  string
	Filename string
}

var LilypondFilesToMake chan LilypondFileToMakeStr

func GetModifiedFilename(s string) string {
	/*
		TODO: do this with a for loop instead
	*/
	return strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(strings.ReplaceAll(s, "/", ""), "\\", ""), "'", ""), "\"", "")
}

func GetLilypond(w http.ResponseWriter, r *http.Request) {
	if FullConfig.LilyRenderEveryTime {
		GetLilypondRET(w, r)
	} else {
		GetLilypondOriginal(w, r)
	}
}

var LilypondRetMutex sync.Mutex

func GetLilypondRET(w http.ResponseWriter, r *http.Request) {
	/*
		This will become the norm in
		an upcoming major update.

		Render the lilypond on each request
		to the wvlist daemon. (assume that
		virtual hosting caching is being
		used)
	*/

	LilypondRetMutex.Lock()
	defer LilypondRetMutex.Unlock()

	query := r.URL.Query()

	if len(query["id"]) != 1 || len(query["no"]) != 1 {
		http.Error(w, "400 Bad Request", 400)
		return
	}

	id := query["id"][0]
	no := query["no"][0]

	// Remove those keys and see if there are other queries (bad request)
	delete(query, "id")
	delete(query, "no")
	if len(query) != 0 {
		http.Error(w, "400 Bad Request", 400)
		return
	}

	/*
		Read the incipit command from the file
	*/

	fpath := "./current/" + id + ".csv"

	contents, err := os.ReadFile(fpath)
	if err != nil {
		log.Println("getting lilypond file error", err.Error())
		return
	}

	reader := csv.NewReader(bytes.NewReader(contents))
	fileContents, err := reader.ReadAll()

	/*
		Check that the row is not out of bounds
	*/

	noInt, err := strconv.Atoi(no)
	if err != nil {
		http.Error(w, "400 bad request", 400)
		return
	}

	if noInt >= len(fileContents) {
		http.Error(w, "400 bad request", 400)
		return
	}
	incipit := fileContents[noInt][3]
	log.Println(incipit)

	lilypondOut, err := os.CreateTemp("", "lilyout*")
	if err != nil {
		http.Error(w, fmt.Sprintf("500 internal error %s", err.Error()), 500)
		return
	}

	err, status := CreateLilypondIncipit(incipit, lilypondOut.Name())
	if err != nil || status != 0 {
		http.Error(w, fmt.Sprintf("500 lilypond returned status code %d: %+v", status, err), 500)
		return
	}

	/*
		Read the input from the png file into a buffer
		which will be served by http.ServeContent
	*/

	pngFilename := lilypondOut.Name() + ".png"

	png, err := os.ReadFile(pngFilename)
	if err != nil {
		http.Error(w, fmt.Sprintf("500 error: %+v", err), 500)
		return
	}

	go func(fnamer string) {
		fname := path.Base(fnamer)
		log.Println("filename to delete", fname)
		// Delete all lilypond files

		matches, err := fs.Glob(os.DirFS(os.TempDir()), fname+"*")
		if err != nil {
			log.Printf("error during glob: %+v", err)
			return
		}

		for _, match := range matches {
			f := path.Join(os.TempDir(), match)
			if err = os.Remove(f); err != nil {
				log.Printf("Error during remove file %s, path %s: %+v", match, f, err)
				return
			}
			log.Printf("Deleted file %s, path %s", match, f)
		}

	}(lilypondOut.Name())

	// http serve content
	http.ServeContent(w, r, "incipit.png", time.Now(), bytes.NewReader(png))

}

func GetLilypondOriginal(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()

	if len(query["id"]) != 1 || len(query["no"]) != 1 {
		http.Error(w, "400 Bad Request", 400)
		return
	}

	/*
		Is a valid request.
	*/

	id := query["id"][0]
	no := query["no"][0]

	/*
		Remove those keys and see if there are other queries
		(bad request)
	*/
	delete(query, "id")
	delete(query, "no")
	if len(query) != 0 {
		http.Error(w, "400 Bad Request", 400)
		return
	}

	/*
		Safely continue with id no
	*/

	buf := new(bytes.Buffer)

	fmt.Fprintf(buf, "%s%s.%s.png", LilypondDirPath, id, no)
	LilypondImageFilename := buf.String()

	if _, err := os.ReadFile(buf.String()); err != nil && os.IsNotExist(err) {
		// File doesn't exist.
		go func() {
			var newFile LilypondFileToMakeStr

			/*
				Read the incipit command from the file
			*/

			fpath := "./current/" + id + ".csv"

			contents, err := os.ReadFile(fpath)
			if err != nil {
				log.Println("getting lilypond file error", err.Error())
				return
			}

			r := csv.NewReader(bytes.NewReader(contents))
			fileContents, err := r.ReadAll()
			log.Println(fileContents)

			/*
				Check that the row is not out of bounds
			*/

			noInt, err := strconv.Atoi(no)
			if err != nil {
				http.Error(w, "400 bad request", 400)
				return
			}

			if noInt >= len(fileContents) {
				http.Error(w, "400 bad request", 400)
				return
			}
			incipit := fileContents[noInt][3]
			log.Println(incipit)

			newFile.Command = incipit
			newFile.Filename = LilypondImageFilename
			LilypondFilesToMake <- newFile
		}()

	} else {
		http.ServeFile(w, r, LilypondImageFilename)
	}
}

func LilypondWriteIncipitsFromChannel() {
	/*
		This function is run in a subroutine.
		For each entry recieved, write the lilypond
		incipit to the file.
	*/
	for true {
		var newFile LilypondFileToMakeStr
		newFile = <-LilypondFilesToMake

		/*
			First check if this file may have already been made
		*/

		if _, err := os.ReadFile(newFile.Filename); !(err != nil && os.IsNotExist(err)) {
			continue
		}

		filename := strings.TrimLeft(strings.TrimRight(newFile.Filename, ".png"), LilypondDirPath) // .png is appended by lilypond
		CreateLilypondIncipit(newFile.Command, filename)
	}

}

func OldGetLilypond(w http.ResponseWriter, r *http.Request) {
	// serve lilypond png file
	pathSan := (r.URL.Query()["f"])
	if len(pathSan) != 1 {
		http.Error(w, "400 Bad request", 400)
		return
	}
	pathSanEscaped := html.UnescapeString(pathSan[0])
	filepathWD := GetModifiedFilename(pathSanEscaped)
	filepath := "./lilypond/" + filepathWD
	file, err := os.Open(filepath)
	file.Close()
	if errors.Is(err, os.ErrNotExist) {
		fmt.Fprintln(w, pathSanEscaped, filepathWD)
		go CreateLilypondIncipit(strings.TrimRight(pathSanEscaped, ".png"), strings.TrimRight(filepathWD, ".png"))
		http.Error(w, "404 not found", 404)
		return

	}
	http.ServeFile(w, r, filepath)
}
