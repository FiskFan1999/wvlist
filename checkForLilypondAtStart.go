/*
Check if the lilypond library is
linked correctly (binary is
listed correctly in config.json)
and give a warning if it is not
(but do not stop the program
immediately)
*/
package main

import (
	"os/exec"
)

func CheckForLilypondAtStart(pathToLily string) (output []byte, err error) {
	command := exec.Command(pathToLily, "--version")
	output, err = command.CombinedOutput()
	return
}
