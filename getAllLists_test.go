package main

import (
	"path/filepath"
	"testing"

	"github.com/google/go-cmp/cmp"
)

var ExpectedOutput []FullListIndex = []FullListIndex{
	{"aaaa, Composer", "aaaa, Composer", "10"},
	{"cdef, Another", "cdef, Another", "20"},
	{"back, Sorter", "zback, Sorter", "30"},
}

func TestGetAllLists(t *testing.T) {
	var index []FullListIndex = GetAllLists(filepath.Join(".testing", "current1"))

	if cmp.Equal(index, ExpectedOutput) {
		return
	}

	t.Logf("%+v", index)

	if len(ExpectedOutput) != len(index) {
		t.Errorf("Output from GetAllLists does not match length of expected output. Expected %d got %d.", len(ExpectedOutput), len(index))
	}

	for i := 0; i < len(ExpectedOutput) && i < len(index); i++ {
		if !cmp.Equal(ExpectedOutput[i], index[i]) {
			t.Errorf("%d Expected: Composer \"%s\", Sort \"%s\", Path \"%s\"", i, ExpectedOutput[i].Name, ExpectedOutput[i].NameSort, ExpectedOutput[i].Path)
			t.Errorf("%d      Got: Composer \"%s\", Sort \"%s\", Path \"%s\"", i, index[i].Name, index[i].NameSort, index[i].Path)
		}
	}
}
