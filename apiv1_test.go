package main

import (
	"bytes"
	"encoding/json"
	"io"
	"io/fs"
	"net/http"
	"net/http/httptest"
	"os"
	"path"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
)

type UploadUglyFailPayload struct {
	Payload  string
	Response map[string]string
}

var (
	UploadUglySuccessfulPayload  = []byte(`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`)
	UploadUglySuccessfulResponse = map[string]string{
		"message": "Thank you for your submission.",
		"status":  "success",
	}
	UploadUglySuccessUnverified = map[string]interface{}{
		"ComposerFirst": "firstname",
		"ComposerLast":  "lastname",
		"ComposerBirth": float64(1900),
		"ComposerDeath": float64(2000),
		"Notes":         "This is the note.\r\nNew line.",
		"SubmitName":    "Submitter name",
		"SubmitEmail":   "",
		"CompositionList": []interface{}{
			map[string]interface{}{"Classifier": "1", "Number": "2", "Title": "3", "Incipit": "c4", "ID": "", "RowNumber": float64(0)},
			map[string]interface{}{"Classifier": "5", "Number": "6", "Title": "7", "Incipit": "d4", "ID": "", "RowNumber": float64(0)}}}

	UploadUglyFailPayloads = []UploadUglyFailPayload{
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4 d4"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "Illegal wvlist order",
			"status":  "error",
		}}, // extra incipit
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4"},{"name":"classification","value":"15"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "Illegal wvlist order",
			"status":  "error",
		}}, // extra classification
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"number","value":"16"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "Illegal wvlist order",
			"status":  "error",
		}}, // extra number
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "Illegal wvlist order",
			"status":  "error",
		}}, // missing
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"nfame":"title","value":"3"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "400 bad request: Illegal json POSTed, does not follow AJAX serialized array syntax",
			"status":  "error",
		}}, // incorrectly spelled "name" -> "nfame"
		{`[{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1900"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "400 bad request: Illegal json POSTed, required field composerLast not included.",
			"status":  "error",
		}},
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"hello"},{"name":"death","value":"2000"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4 d4"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "400 bad request: Field birth must be an integer.",
			"status":  "error",
		}}, // non integer value for birth
		{`[{"name":"composerLast","value":"lastname"},{"name":"composerFirst","value":"firstname"},{"name":"birth","value":"1999"},{"name":"death","value":"non integer"},{"name":"notes","value":"This is the note.\r\nNew line."},{"name":"submitname","value":"Submitter name"},{"name":"email","value":""},{"name":"classification","value":"1"},{"name":"number","value":"2"},{"name":"title","value":"3"},{"name":"incipit","value":"c4 d4"},{"name":"incipit","value":"c4"},{"name":"classification","value":"5"},{"name":"number","value":"6"},{"name":"title","value":"7"},{"name":"incipit","value":"d4"}]`, map[string]string{
			"message": "400 bad request: Field death must be an integer.",
			"status":  "error",
		}}, // non integer value for death
	}
)

func getTestApiHandler(temporaryCurrent, temporarySubmissions string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		APIv1HandlerPath(w, r, temporaryCurrent, temporarySubmissions)
	}
}

func TestApiV1SubmitCheckFails(t *testing.T) {
	/*
		Regression test. Check that for several
		different types of bad inputs, that they
		are correctly rejected, and no files are
		written to submissions/ or current/
	*/
	var submissionsDir string
	var currentDir string
	var err error
	submissionsDir, err = os.MkdirTemp("", "submissions_test.*")
	if err != nil {
		t.Fatalf("Error creating temporary submissions dir: %+v", err)
	}
	defer os.RemoveAll(submissionsDir)

	currentDir, err = os.MkdirTemp("", "current_test.*")
	if err != nil {
		t.Fatalf("Error creating temporary current dir: %+v", err)
	}
	defer os.RemoveAll(currentDir)

	/*
		Create http client requests
	*/
	testHandler := getTestApiHandler(currentDir, submissionsDir)

	for n, pr := range UploadUglyFailPayloads {
		request, err := http.NewRequest(http.MethodPost, "/api/v1/uploadugly", strings.NewReader(pr.Payload))
		if err != nil {
			t.Fatalf("Error while creating request: %+v", err)
		}
		w := httptest.NewRecorder()
		testHandler(w, request)

		// read result
		var result *http.Response
		result = w.Result()
		defer result.Body.Close()

		// this api endpoint returns 200 even if it fails
		// failure if reflected in the JSON in body.
		if result.StatusCode != 200 {
			t.Fatalf("Regression test failed. For incorrect payload, should have returned status code 200 but returned %s.", result.Status)
		}

		// read body
		bodyBytes, err := io.ReadAll(result.Body)
		if err != nil {
			t.Fatalf("Error reading response body: %+v", err)
		}
		var bodyJson map[string]string
		if err = json.Unmarshal(bodyBytes, &bodyJson); err != nil {
			t.Fatalf("Error unmarshaling json: %+v", err)
		}

		// check that the recieved status JSON and expected match up

		if !cmp.Equal(bodyJson, pr.Response) {
			t.Log(cmp.Diff(pr.Response, bodyJson))
			t.Errorf("For fail input #%d, did not recieve correct response.", n)
		}

		// check that there are no files in current or submissions.
		subfiles, err := os.ReadDir(submissionsDir)
		if err != nil {
			t.Fatalf("Erorr while reading submissions directory: %+v", err)
		}
		if len(subfiles) != 0 {
			t.Fatalf("Error: after illegal uploadugly submission, submissions/ directory should not have any file but has %d.", len(subfiles))
		}

		curfiles, err := os.ReadDir(currentDir)
		if err != nil {
			t.Fatalf("Erorr while reading current directory: %+v", err)
		}
		if len(curfiles) != 0 {
			t.Fatalf("Error: after illegal uploadugly submission, current/ directory should not have any file but has %d.", len(curfiles))
		}
	}
}

func TestApiV1SubmitReg(t *testing.T) {

	/*
		Regression test

		Test http request to /api/v1/uploadugly
		with valid payload and check that
		this gets saved in /submissions correctly
	*/

	var submissionsDir string
	var currentDir string
	var err error
	submissionsDir, err = os.MkdirTemp("", "submissions_test.*")
	if err != nil {
		t.Fatalf("Error creating temporary submissions dir: %+v", err)
	}
	defer os.RemoveAll(submissionsDir)

	currentDir, err = os.MkdirTemp("", "current_test.*")
	if err != nil {
		t.Fatalf("Error creating temporary current dir: %+v", err)
	}
	defer os.RemoveAll(currentDir)

	/*
		Create http client request
	*/
	testHandler := getTestApiHandler(currentDir, submissionsDir)
	correctBodyRequest, err := http.NewRequest(http.MethodPost, "/api/v1/uploadugly", bytes.NewReader(UploadUglySuccessfulPayload))
	if err != nil {
		t.Fatalf("Error creating temporary submissions dir: %+v", err)
	}
	w := httptest.NewRecorder()
	testHandler(w, correctBodyRequest) // request

	// read result
	var result *http.Response
	result = w.Result()
	defer result.Body.Close()

	// successful. should return 200
	if result.StatusCode != 200 {
		t.Fatalf("Regression test failed. For correct payload, should have returned status code 200 but returned %s.", result.Status)
	}
	bodyBytes, err := io.ReadAll(result.Body)
	if err != nil {
		t.Fatalf("Error reading response body: %+v", err)
	}
	t.Logf("body: %s", bodyBytes)

	var bodyJsonSuccessful map[string]string

	if err = json.Unmarshal(bodyBytes, &bodyJsonSuccessful); err != nil {
		t.Fatalf("Error unmarshaling response body into JSON: %+v", err)
	}

	if !cmp.Equal(bodyJsonSuccessful, UploadUglySuccessfulResponse) {
		t.Log(cmp.Diff(bodyJsonSuccessful, UploadUglySuccessfulResponse))
		t.Fatalf("Regression test fail: Wrong body response for successful submission: expected %v, got %v", bodyJsonSuccessful, UploadUglySuccessfulResponse)
	}

	/*
		As part of regression test, check for two files
		in submissions/ (submission info and password)
		and none in current/
	*/
	currentDirContents, err := os.ReadDir(currentDir)
	if err = json.Unmarshal(bodyBytes, &bodyJsonSuccessful); err != nil {
		t.Fatalf("Error readind temporary current directory: %+v", err)
	}
	if len(currentDirContents) != 0 {
		t.Errorf("Regression test fail: After /api/v1/uploadugly current/ directory should be empty but has %d files.", len(currentDirContents))
		for i, name := range currentDirContents {
			t.Errorf("File %d: Name \"%s\"", i, name.Name())
		}
	}

	submissionsDirContents, err := os.ReadDir(submissionsDir)
	if err = json.Unmarshal(bodyBytes, &bodyJsonSuccessful); err != nil {
		t.Fatalf("Error readind temporary submissions directory: %+v", err)
	}

	if len(submissionsDirContents) != 2 {
		t.Errorf("Regression test fail: After /api/v1/uploadugly submissions/ directory should have 2 files but has %d files.", len(submissionsDirContents))
		for i, name := range submissionsDirContents {
			t.Errorf("File %d: Name \"%s\"", i, name.Name())
		}
		t.FailNow()
	}
	/*
		Read the unverified submission in submissions/
	*/

	subFS := os.DirFS(submissionsDir)
	unverifiedGlob, _ := fs.Glob(subFS, "*.unverified")

	if len(unverifiedGlob) != 1 {
		t.Errorf("Regression test: after successful /api/v1/uploadugly, should be one file *.unverified in submissions/, but are %d.", len(unverifiedGlob))
		for i, g := range unverifiedGlob {
			t.Errorf("File %d: Name \"%s\"", i, g)
		}
		t.FailNow()
	}

	unverifiedFile, err := os.ReadFile(path.Join(submissionsDir, unverifiedGlob[0]))
	if err != nil {
		t.Fatalf("Error reading unverified submission in submissions/: %+v", err)
	}

	var unverifiedFileJSON map[string]interface{}
	if err = json.Unmarshal(unverifiedFile, &unverifiedFileJSON); err != nil {
		t.Fatalf("Error unmarshaling unverified file into JSON: %+v", err)
	}

	t.Logf("%s", unverifiedFile)
	t.Logf("%+v", unverifiedFileJSON)
	t.Logf("%+v", UploadUglySuccessUnverified)

	if !cmp.Equal(unverifiedFileJSON, UploadUglySuccessUnverified) {
		t.Log(cmp.Diff(unverifiedFileJSON, UploadUglySuccessUnverified))
		t.Fatal("Regression fail: .unverified file does not match expected.")
	}

}
