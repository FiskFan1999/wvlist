# API endpoints documentation

##### *2022, William Rehwinkel*

----

## v1

### `admin`
`GET /api/v1/admin?command=`
> Note: `/api/v1/admin` returns status code `200 OK` even if the administrator command itself has an error, such as running `vs <num>` for an invalid number. The text output should be interpreted by the administrator for the success or failure of the operation.

Executes an administrator command as if it were entered in the admin console. For example, here is an example of how to access this api endpoint using `curl`:
```bash
curl --user "user:pass" -G "https://example.com/api/v1/admin" --data-urlencode "command=ls -a"
```

Parameters:

- Requires http basic authentication, valid administrator username and password.
- command (parameter): url-encoded command as if typed into the administrator console (`/admin`)

Responses:
- `200 OK` - Handles administrator command. Returns the text output in body.
- `401 unauthorized` - Incorrect login credentials


### `verify`
`GET /api/v1/verify/id/password`
*Verify a submission using the randomly generated password that was sent to the user via email. Note that the completed link to this endpoint including filled out id and password is sent to the user upon supplying their email address.*
Parameters:

- `id` (path): the id of the submission.
- `password` (path): the randomly generated password.

Responses:
- `200 OK` - The verification was completed successfully.
- `400 Bad Request` - The user supplied an incorrect id or password.
- `500 Internal Server Error`

### `verifyedit`
`GET /api/v1/verifyedit/id/password`
*Verify an edit using the randomly generated password that was sent to the user via email. Note that the completed link to this endpoint including filled out id and password is sent to the user upon supplying their email address.*
Parameters:

- `id` (path): the id of the edit.
- `password` (path): the randomly generated password.

Responses:
- `200 OK` - The verification was completed successfully.
- `400 Bad Request` - The user supplied an incorrect id or password.
- `500 Internal Server Error`

### `uploadugly`
`POST /api/v1/uploadugly`

> Note: this api endpoint is only meant for internal use by the wvlist submissions page. The syntax is obtuse due to the limitations of how the browser collects the information from the submission page via javascript, and other users should not try to replicate this using their own services. A seperate endpoint to accept uploads using a more friendly syntax will be added shortly.

Parameters:
- Body: JSON object representing the submission.

Status Code:
- `200 OK` - Successful
- `400 Bad Request` - Some incorrect syntax in the JSON body. Note that this should NEVER happen for a submission which is properly constructed using the submission page.
- `405 Method Not Allowed` - non-POST request
- `500 Internal Server Error`

### `uploadeditugly`
`POST /api/v1/uploadeditugly`

> Note: this api endpoint is only meant for internal use by the wvlist edit page. The syntax is obtuse due to the limitations of how the browser collects the information from the edit page via javascript, and other users should not try to replicate this using their own services. A seperate endpoint to accept uploads using a more friendly syntax will be added shortly.

Parameters:
- Body: JSON object representing the edit.

Status Code:
- `200 OK` - Successful
- `400 Bad Request` - Some incorrect syntax in the JSON body. Note that this should NEVER happen for an edit which is properly constructed using the edit page.
- `405 Method Not Allowed` - non-POST request
- `500 Internal Server Error`