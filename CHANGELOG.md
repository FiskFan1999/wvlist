## v1.3.1
8 August 2022: This patch fixes a regression that was unfortunately introduced in v1.3.0 to do with the edit submission page. It also adds a new file, `CHANGELOG.md` to preserve the logs of changes between versions.

Bugs:
- The edit page did not work at all. This was due to the script link which would have loaded jquery being empty.

Internal changes:
- [`CHANGELOG.md`](https://codeberg.org/FiskFan1999/wvlist/src/commit/6018ce7d4344d33bd069199d1e64ec11a5239bf9/CHANGELOG.md) will retain all release and patch notes.

## v1.3.1-rc1
4 August 2022: This upcoming patch release fixes a regression in v1.3.0 in which the page to edit composition lists does not work at all (the link to jquery is broken). This version also includes changes to the README.md page and internal changes to the /submit and /edit pages.

## v1.3.0
26 July 2022: We are now proud to release the next minor update for wvlist.

This update does not have any backwards-compatibility-breaking changes, however the administrator is advised to backup all files before upgrading.

_Note: for now, the wvlist application still needs to be run with the git repository as the working directory. However, this will be hopefully be changed in an upcoming minor or major update, at that point it will be possible to run as a standalone binary._

## v1.3.0-rc1
6 July 2022: We have prepared the next minor update for wvlist and am presenting the first release candidate.

Please be sure to back-up all files before upgrading to this release candidate.

User and administrator facing changes:

-   `./wvlist password` uses new TUI.
-   Administrator console is not blocked on plaintext anymore.
-   Added opt-in behavior in which lilypond re-renders incipits on each request and doesn't save them. This is for use with nginx caching. See [`d96fa22fa6/config.default.json (L8)`](https://codeberg.org/FiskFan1999/wvlist/src/commit/d96fa22fa6376547ad4e6993a424c92d92c26a83/config.default.json#L8) and [`d96fa22fa6/config.txt (L20)`](https://codeberg.org/FiskFan1999/wvlist/src/commit/d96fa22fa6376547ad4e6993a424c92d92c26a83/config.txt#L20)
-   Extended README.md and added `manual/api.md` for API documentation.
-   Added optional config.json option to specify the URL link to the jquery javascript file to use. See [`d96fa22fa6/config.default.json (L14)`](https://codeberg.org/FiskFan1999/wvlist/src/commit/d96fa22fa6376547ad4e6993a424c92d92c26a83/config.default.json#L14) and [`d96fa22fa6/config.txt (L36)`](https://codeberg.org/FiskFan1999/wvlist/src/commit/d96fa22fa6376547ad4e6993a424c92d92c26a83/config.txt#L36)

Bugfixes:

-   Some illegally formatted payloads that would get accepted by `/api/v1/uploadugly` are now rejected.

Internal changes:

-   More regression and unit tests
-   Most file paths use `path.Join()` now.
-   Template directory is embedded now.
-   Some log.Println are removed.

User and administrator facing changes:

-   ./wvlist password uses new TUI.
-   Administrator console is not blocked on plaintext anymore.
-   Added opt-in behavior in which lilypond re-renders incipits on each request and doesn't save them. This is for use with nginx caching. See d96fa22fa6/config.default.json (L8) and d96fa22fa6/config.txt (L20)
-   Extended README.md and added manual/api.md for API documentation.
-   Added optional config.json option to specify the URL link to the jquery javascript file to use. See d96fa22fa6/config.default.json (L14) and d96fa22fa6/config.txt (L36)

Bugfixes:

-   Some illegally formatted payloads that would get accepted by /api/v1/uploadugly are now rejected.

Internal changes:

-   More regression and unit tests
-   Most file paths use path.Join() now.
-   Template directory is embedded now.
-   Some log.Println are removed.

## v1.2.2
7 June 2022: This patch update modifies the behavior of the administrator console to be more user-friendly and forgiving, and brings internal changes to the code that, while not affecting behavior or performance, will make the codebase more flexible and easier to write tests for and adapt in the upcoming major update.

### Modifications

-   Administrator commands are not forgiving towards including multiple spaces between args (use `strings.Fields` instead of `strings.Split`).

### Internal changes:

-   API v1 handler uses variables for location of `current/` and `submissions/` directories
-   Delete `.github` directory, as workers are not present on codeberg and that behavior is included in `./wvtest`.

## v1.2.1
18 April 2022: This patch fixes a panic when building wvlist from a directory that does not have a .git directory, thus not having a value for the commit.

## v1.2.0 codeberg
13 April 2022: This is a re-release of version 1.2.0 which was previously released on Github (previous releases can be found there). However, this release also contains two bugfixes from v1.2.0 (Github).

-   Fix bug in which entering password via `./wvlist password` over STDIN did not read the input.
-   Lilypond version and logic which mkdir's necessary directories is not performed unless explicitly running `./wvlist run`.

## v1.2.0
13 April 2022: This minor update brings a new API endpoint for instance administrators and a number of internal changes.

-   API endpoint `/api/v1/admin` executes administrator commands without the need to log in to the browser administrator console. (Includes example script `/adminCli` which implements this API call)
-   On homepage menu, "Github" -> "Git"

Internal changes:

-   Updated all template HTML pages according to html linter errors
-   Updated README
-   Added workflow to run unit tests and html linter

## v1.2.0-rc2
8 April 2022: Changelog

-   On homepage menu, `Github` -> `Git`
-   Added script to run tests and linters, for ease of use to replicate functionality of dedicated runners.

## v1.2.0-rc1
5 April 2022: This minor update adds functionality to access the administrator console via an API endpoint, and modifies all HTML templates.

-   Adds an API endpoint `/api/v1/admin` to execute commands via the administrator console. Includes a script `./adminCli` which is a basic example of how to utilize this endpoint.
-   Modifies HTML code to be more correct (such as double-quoting all attributes), according to an HTML-linter.

## v1.1.0
25 March 2022
### Change-log

-   Added lastsort to .json for composers who should not be sorted by the first letter in their last name (such as the Dutch composer van Noordt)
-   Lilypond incipits render correctly (template file changed slightly). Lyrics are not too big, for example.
-   List of compositions uses font family monospace.
-   Og (open graph) information added to most pages.
-   Favicons added, uses logo created by [@FiskFan1999](https://github.com/FiskFan1999).
-   Admin command `redit` to reject edit submissions.
-   Some user-facing text changes throughout the service.

### Internal changes

-   Listening net.http.muxers are created in separate functions.
-   Internal logging to console is done via `log` instead of `fmt.Println` (Thanks [@sauravmanoj](https://github.com/sauravmanoj)!)

## v1.1.0-rc2
14 March 2022: Changes from release-candidate 1:

-   Fixes bug where password for edit submission doesn't get deleted upon admin command `redit`
-   Table headers are not font-family monospace. Table headers and table contents font-family is specified in `style.css`.

## v1.1.0-rc1
14 March 2022: v1.1.0 changelog:

-   (Internal) Use log.Print instead of fmt.Print for debugging information that is printed to console. (Thank you!)
-   Each WV entry's .json file can now include "lastsort", which will sort by this instead of by the last name which is displayed.
-   Lilypond incipits are not resized properly, some things like lyrics are not too large anymore.
-   Use monospace font family for the table on each catalogue page.
-   Add favicons to each page.
-   Administrator command `redit` to reject edit submissions.
-   Assorted text changes throughout the site.

## v1.0.4
12 March 2022: Bugfixes:

-   Console will log and exit on http.ListenAndServe error.
-   Admin command vedit now prints out the note that was written in the edit submission.
-   Some temporary files are made with 600 permissions like before v1.0.3.

## v1.0.3
2 March 2022: Bugfixes:

-   All new files created by daemon can be read by the administrator without `sudo` priviledges required. This rectifies previous behavior where some files (created with `os.CreateTemp`) had perms 600.
-   Do not write onion-address header in response to plaintext responses. This rectifies previous behavior in which the onion-address was sent over plaintext as well, even though it was not parsed by the Tor browser and should not be trusted to MITM concerns.
-   Fixes a bug in which `rsub` causes the damon to panic when no args are passed.
-   All admin commands check for exact number of required args. Past behavior: checking for greater than or equal to required number of args and silently ignoring those args which are beyond the required number.

## v1.0.2
25 February 2022: This patch includes two bugfixes. Please note that when updating to this patch you will be warned that no path to lilypond is supplied in config.json. Please add the following line to config.json (reflected in config.default.json): `"lilypond_path": "lilypond",` (be sure to follow proper syntax).

-   As reported by tacerus in [#5](https://github.com/FiskFan1999/wvlist/issues/5) some directories which are necessary but are not included in the repository (because they start as empty) will lead to runtime panics. wvlist damon now checks and makes the directories if needed (so in this aspect, the damon should work from a fresh installation).
-   Added a check at the start of runtime to see if a valid path to lilypond is provided. This will give a warning on bootup but the daemon will continue to run.

Note: I know that documentation for this project is very poor right now. This is just being worked on in my free time over winter break and while I'm at university, and writing thorough enough docs is not one of my specialties anyway. Please feel free to pester me with as many questions as you need on IRC if you are having problems while installing and hosting this yourself. Hopefully I'll add more documentation and installation instructions for a future minor update.

## v1.0.1
22 February 2022: Bugfixes:

-   Sort by unidecode instead of by string only (sorting names with accents would not be sorted correctly)
-   Edit submission doesn't fail when no email is supplied (copies behavior when not supplying an email for new submission)

## v1.0.0
17 February 2022: 
After testing for the last few days and finding no known bugs or issues, I am ready to release a stable version of wvlist, v1.0.0.

This version has no changes from v1.0.0-rc2 except more information in config.default.json and config.txt to include all the configuration values that I have enabled so far. It is live at [https://wvlist.net](https://wvlist.net) and as a hidden service at [http://xtjuwqe4hlqojbknh2kwlm5nh4usj2yh5ceuh2x4gy27wirurxh3qoid.onion/](http://xtjuwqe4hlqojbknh2kwlm5nh4usj2yh5ceuh2x4gy27wirurxh3qoid.onion/)

At this point, I have finished the initial development that I was planning to do over winter break, over the next weeks I will compile a list of future planned features, which will most likely all be minor updates. Of course, bug fixes will be released as patches.

While I am running this as my own service, you are of course welcome to fork this project or host it yourself. In the future, I am planning to implement more API endpoints, such as a way to export all wvlists from one instance in a format that can be imported to another instance.

As always, bug reports are welcome either via github issue or by email ([william@wvlist.net](mailto:william@wvlist.net)).

## v1.0.0-rc2
16 February 2022: Change:

Added item in config, "tor_address". If this is not an empty string, the server will set the "Onion-Location" header, which will make the tor address be suggested if the website is joined on the Tor browser.

## v1.0.0-rc1
14 February 2022: All planned features have been added. This will become v1.0.0 barring any discovered bugs in the next few days.

NOTE: commit [bc667f9](https://github.com/FiskFan1999/wvlist/commit/bc667f9722a1cde7ebffec8a2e259cd2d740e607) added a tab on the homepage which lists the administrators listed in config.json as well as their email address. If you would like to hide the email address, set the "hideemail" value to true. Such an admin block would look like this:
```json
"admins": [
  {
    "name": "William Rehwinkel",
    "email": "email@example.net", 
    "hideemail": true,
    "username": "william",
    "hash": "$2a$15hash"
  }
]
```
Changelog:

-   Cleaned up pages, adding explaining text on submit and lilypond sandbox, found in `constText.go`
-   Added administrator tab to homepage, listing administrators and their email address.

## Beta versions
- **v0.5.2**: 14 February 2022: minor feature upgrade: verification email for submissions and edits includes attachments describing the submission in question.
- **v0.5.1**: 14 February 2022: Bug fix: attempting to submit an edit results in API giving success on no errors (previously returned an error anyway).
- **v0.5.0**: 12 February 2022: This is a VERY unstable build, but I have just added the final features that I was intending for. So this might be the last update before I get to release candidates and then stable builds.
Changelog:
  -   Added administrator commands `vedit` `aedit` and `deletelily`
  -   Administrator can view and accept edits, which perform a patch on the .csv file and append the note written by the contributor to the .notes file.
  -   `deletelily` command deletes all lilypond engraved files, quick way to fix incipits not appearing on correct line for now (will implement better solution in the future).
- **v0.4.1**: 9 February 2022: Bugfix: on submit page, clicking "insert above this row" button inserts row with "Up", "Do." etc. instead of one-character symbols for buttons.
- **v0.4.0**: 7 February 2022: Changelog:
  - Admin reject command `rsub`
  - /api/v1/uploadugly returns errors in JSON which is read by AJAX javascript
  - Span listing version and commit links to that commit on github
- **v0.3.0**: 7 Febuary 2022: Note that to upgrade to this version you need to add the following line to `config.json`
`"lilypond_timeout": "10s"` or some other value. (Refer to `config.txt`)
Changelog:

  -   Lilypond timeout reads from config.json
  -   API endoint uploadugly returns errors in json form that are read by AJAX
Bug fixes:
  -   Don't print simple auth username and password to STDOUT anymore.
- **v0.2.0**: 6 February 2022: v0.2.0 changelog:
Admin command `testemail <address>` sends an email to test SMTP settings, the same way as `./wvlist sendemail`.
Admin `ls` command prints number of verified and unverified submissions.
Homepage elements are sorted by "Composer last name, Composer first name" instead of previously arbitrarily sorted by random id of filename generated by os.CreateTemp
Changes to submit html page: Add button on each row to insert blank row above that row. Use arrows and single command instead of words on each button.
- **v0.1.0**: 5 February 2022: This is the first alpha version of wvlist, and the first version which will be publicly available on my instance, [https://wvlist.net/](https://wvlist.net/)
The completed features are
  -   Ability to view individual submissions (/view)
  -   Homepage which lists all submissions, including search (/)
  -   Admin console, with some basic commands (ls, vsub, asub)
  -   Page for user submissions, which POSTs to an API endpoint and sends email
  -   Rudimentary /api/v1 endpoints, currently only /uploadugly and /verify (more api endpoints, to do, among other things, sharing submission information via json, are planned)
Please remember that this is an early unstable build. Expect more patch updates to appear within the next weeks, as well as more features.
