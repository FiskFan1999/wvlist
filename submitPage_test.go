package main

import (
	//"net/http"
	"bytes"
	"context"
	"fmt"
	"net/http/httptest"
	"os"
	"path"
	"testing"
	"time"

	"github.com/chromedp/chromedp"
	"golang.org/x/crypto/bcrypt"
)

func TestSubmitProcess(t *testing.T) {
	bcryptBytes, err := bcrypt.GenerateFromPassword([]byte("pass"), bcrypt.MinCost)
	if err != nil {
		t.Fatal(err.Error())
	}

	FullConfig = &ConfigStr{
		Jquery: "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
		Admins: []ConfigAdminInfo{
			{
				"admin",
				"admin@example.net",
				false,
				"admin",
				string(bcryptBytes),
			},
		},
	}

	// create empty directory for /current and /submissions
	tempDir, err := os.MkdirTemp("", "*")
	if err != nil {
		t.Fatal(err.Error())
	}
	defer os.RemoveAll(tempDir)

	for _, d := range []string{
		"current",
		"submissions",
		"lilypond",
	} {
		if err := os.Mkdir(path.Join(tempDir, d), 0760); err != nil {
		}
	}

	mux := GetMux(true, path.Join(tempDir, "current"), path.Join(tempDir, "submissions"), path.Join(tempDir, "lilypond"))

	testServer := httptest.NewServer(mux)
	defer testServer.Close()

	// create context
	// ctx, cancel := chromedp.NewContext(context.Background(), chromedp.WithDebugf(t.Logf))
	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// var alertText []byte
	// var confirmText []byte
	var location []byte
	err = chromedp.Run(ctx,
		chromedp.Navigate(fmt.Sprintf("%s/submit/", testServer.URL)),
		chromedp.WaitVisible(`#submit`),
		chromedp.Evaluate(`document.getElementById("composerLast").setRangeText("lastname")`, nil),
		chromedp.Evaluate(`document.getElementById("composerFirst").setRangeText("firstname")`, nil),
		chromedp.Evaluate(`document.getElementById("birth").setRangeText("2000")`, nil),
		chromedp.Evaluate(`document.getElementById("death").setRangeText("3000")`, nil),
		chromedp.Evaluate(`document.getElementById("notes").setRangeText("notes here")`, nil),
		chromedp.Evaluate(`document.getElementById("classification").setRangeText("WV")`, nil),
		chromedp.Evaluate(`document.getElementById("number").setRangeText("1")`, nil),
		chromedp.Evaluate(`document.getElementById("title").setRangeText("Title")`, nil),
		chromedp.Evaluate(`document.getElementById("incipit").setRangeText("c4 e4 g2")`, nil),

		chromedp.Evaluate(`var confirmoutput = "";`, nil),
		chromedp.Evaluate(`alertoutput = "";`, nil),

		chromedp.Evaluate(`window.confirm = function(st){confirmoutput = st; return true;}`, nil), // click the "yes" button on the alert.
		chromedp.Evaluate(`window.alert = function(st){alertoutput = st; return true;}`, nil),     // click the "yes" button on the alert.
		chromedp.Evaluate(`document.getElementById("submit").click()`, nil),                       // click submit button
		chromedp.Sleep(time.Millisecond*500),
		/*
			TODO: why do i get an uncaught error: confirmoutput not defined here?
		*/
		// chromedp.Evaluate(`confirmoutput`, &confirmText),         // click the "yes" button on the alert.
		// chromedp.Evaluate(`alertoutput`, &alertText),             // click the "yes" button on the alert.
		chromedp.Evaluate(`window.location.pathname`, &location), // if successful, == "/"

	)
	if err != nil {
		t.Fatal(err.Error())
	}
	// t.Logf("%s - %s - %s", alertText, confirmText, location)

	/*
		if !bytes.Equal(confirmText, []byte(`"Are you sure you want to submit?"`)) {
			t.Errorf("Did not get correct text in confirm window: %s", confirmText)
		}

		if !bytes.Equal(alertText, []byte(`"The submission was accepted. Thank you."`)) {
			t.Errorf("Did not get correct text in alert window: %s", alertText)
		}
	*/

	if !bytes.Equal(location, []byte(`"/"`)) {
		t.Errorf("Incorrect window location after makeSubmission: %s", location)
	}

}
