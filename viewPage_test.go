package main

import (
	"path/filepath"
	"testing"

	"github.com/google/go-cmp/cmp"
)

var TestParseCurrentSingleCases map[string]map[string]interface{} = map[string]map[string]interface{}{
	"10": {
		"last":     "aaaa",
		"first":    "Composer",
		"lastsort": "",
		"birth":    1901,
		"death":    2001,
		"lock":     "",
		"notes": []Note{
			{
				"This note should be read",
				"William",
				"Jan 1, 2022",
			},
		},
		"wvlist": []WVEntry{
			{
				"No",
				"1",
				"Title",
				"lilypond",
				"10",
				0,
			},
		},
	},
	"30": {
		"last":     "back",
		"first":    "Sorter",
		"lastsort": "zback",
		"birth":    1969,
		"death":    2040,
		"lock":     "",
		"notes": []Note{
			{
				"I'll be interested if the sort works this time",
				"Amy",
				"Apr 20, 2022",
			},
		},
		"wvlist": []WVEntry{
			{
				"11",
				"12",
				"13",
				"14",
				"30",
				0,
			},
			{
				"16",
				"17",
				"18",
				"19",
				"30",
				1,
			},
			{
				"21",
				"22",
				"23",
				"24",
				"30",
				2,
			},
		},
	},
}

func TestParseCurrentSingle(t *testing.T) {
	var id string
	var exp map[string]interface{}
	for id, exp = range TestParseCurrentSingleCases {
		single, err := ParseCurrentSingle(filepath.Join(".testing", "current1"), id)

		if err != nil {
			t.Fatalf("%+v", err)
		}

		/*
			Test for correct values
		*/
		if id != single.ID {
			t.Errorf("id %s: Recieved wrong index, recieved %s", id, single.ID)
		}

		if exp["last"] != single.ComposerLast {
			t.Errorf("id %s: ComposerLast expected \"%s\" recieved \"%s\"", id, exp["last"], single.ComposerLast)
		}

		if exp["first"] != single.ComposerFirst {
			t.Errorf("id %s: ComposerFirst expected \"%s\" recieved \"%s\"", id, exp["first"], single.ComposerFirst)
		}

		if exp["lastsort"] != single.ComposerLastSort {
			t.Errorf("id %s: ComposerLastSort expected \"%s\" recieved \"%s\"", id, exp["lastsort"], single.ComposerLastSort)
		}

		if exp["birth"] != single.ComposerBirth {
			t.Errorf("id %s: ComposerBirth expected \"%d\" recieved \"%d\"", id, exp["birth"], single.ComposerBirth)
		}

		if exp["death"] != single.ComposerDeath {
			t.Errorf("id %s: ComposerDeath expected \"%d\" recieved \"%d\"", id, exp["death"], single.ComposerDeath)
		}

		if exp["lock"] != single.Lock {
			t.Errorf("id %s: Recieved a lock when there shouldn't be one, or vice versa. (Recieved \"%s\")", id, single.Lock)
		}

		if !cmp.Equal(exp["notes"], single.Notes) {
			t.Errorf("id %s: Notes were not correct. Expected %+v recieved %+v", id, exp["notes"], single.Notes)
		}

		if !cmp.Equal(exp["wvlist"], single.WVList) {
			t.Errorf("id %s: WVlist was not correct. Expected %+v recieved %+v", id, exp["wvlist"], single.WVList)
		}
	}

}
