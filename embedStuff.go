package main

import (
	"embed"
)

//go:embed embedtemplate/*
var Templates embed.FS

//go:embed lilypond_template
var LilypondTemplateFS embed.FS
