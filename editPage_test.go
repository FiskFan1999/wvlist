package main

import (
	"bytes"
	"context"
	"fmt"
	"net/http/httptest"
	"os"
	"path"
	"testing"
	"time"

	"github.com/chromedp/chromedp"
	"golang.org/x/crypto/bcrypt"
)

func TestEditSubmitProcess(t *testing.T) {
	testingCurrentDir := path.Join(".testing", "editheadless", "current")
	CurrentDir = testingCurrentDir
	bcryptBytes, err := bcrypt.GenerateFromPassword([]byte("pass"), bcrypt.MinCost)
	if err != nil {
		t.Fatal(err.Error())
	}

	FullConfig = &ConfigStr{
		Jquery: "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js",
		Admins: []ConfigAdminInfo{
			{
				"admin",
				"admin@example.net",
				false,
				"admin",
				string(bcryptBytes),
			},
		},
	}

	// create temporary submissions directory
	tempSubmissionsDir, err := os.MkdirTemp("", "*")
	if err != nil {
		t.Fatal(err.Error())
	}
	defer os.RemoveAll(tempSubmissionsDir)

	mux := GetMux(true, testingCurrentDir, tempSubmissionsDir, tempSubmissionsDir)
	testServer := httptest.NewServer(mux)
	defer testServer.Close()

	ctx, cancel := chromedp.NewContext(context.Background())
	defer cancel()

	// var alertText []byte
	// var confirmText []byte
	var titleValue []byte
	var body []byte
	var location []byte
	err = chromedp.Run(ctx,
		chromedp.Navigate(fmt.Sprintf("%s/edit/%d", testServer.URL, 863986974)),
		// chromedp.Evaluate(`document.getElementById("composerLast").setRangeText("lastname")`, nil),
		chromedp.Evaluate(`document.getElementsByTagName("body")[0].innerText `, &body),
		chromedp.Evaluate(`document.getElementById("title").value`, &titleValue),

		chromedp.Evaluate(`window.confirm = function(){return true;}`, nil), // click the "yes" button on the alert.
		chromedp.Evaluate(`window.alert = function(){return true;}`, nil),   // click the "yes" button on the alert.
		chromedp.Evaluate(`document.getElementById("submit").click()`, nil), // click submit button
		chromedp.Sleep(time.Second),
		chromedp.Evaluate(`window.location.pathname`, &location), // if successful, == "/"

	)
	if err != nil {
		t.Fatal(err.Error())
	}
	if !bytes.Equal(titleValue, []byte(`"Praeludium in E Minor, Gro\u00df"`)) {
		t.Errorf("Incorrect value found in composer table, got \"%s\".", titleValue)
	}
	// t.Logf("%s - %s - %s", alertText, confirmText, location)

	/*
		if !bytes.Equal(confirmText, []byte(`"Are you sure you want to submit?"`)) {
			t.Errorf("Did not get correct text in confirm window: %s", confirmText)
		}

		if !bytes.Equal(alertText, []byte(`"The submission was accepted. Thank you."`)) {
			t.Errorf("Did not get correct text in alert window: %s", alertText)
		}
	*/

	if !bytes.Equal(location, []byte(`"/"`)) {
		t.Errorf("Incorrect window location after makeSubmission: %s", location)
	}
}
