package main

import (
	"bytes"
	"fmt"
	"log"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"golang.org/x/crypto/bcrypt"
)

const BCRYPTCOST = 10

func MakePasswordHashCommand(password string) {
	/*
		Function is run by calling ./wvlist password
	*/
	var passwd1 []byte
	var passwd2 []byte

	if len(password) != 0 {
		passwd1 = []byte(password)
		passwd2 = []byte(password)
	} else {
		/*
			Run tui application
		*/
		mod := InitializePasswordModel()
		if err := tea.NewProgram(mod).Start(); err != nil {
			log.Fatalln("bubbletea error:", err.Error())
		}
		fmt.Println()

		passwd1 = []byte(mod.Pass[0].Value())
		passwd2 = []byte(mod.Pass[1].Value())
	}

	if string(passwd1) != string(passwd2) {
		log.Println("ERROR: you did not enter the same password.")
		MakePasswordHashCommand("") // re run
		return
	}

	if len(passwd1) == 0 || len(passwd2) == 0 {
		log.Println("ERROR: Password must not be empty.")
		// MakePasswordHashCommand("") // re run
		// end program
		return
	}

	hash, err := bcrypt.GenerateFromPassword(passwd1, BCRYPTCOST)
	if err != nil {
		log.Println("BCRYPT ERROR:", err.Error())
		return
	}
	if len(password) == 0 {
		// extra formatting
		fmt.Println("hash: (enter the following has in the \"password\" field of the admin table in config.json)")
		fmt.Println()
	}

	fmt.Println(string(hash))

	if len(password) == 0 {
		fmt.Println()
	}
}

/*
Bubble tea stuff
*/
var (
	focusedStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("205"))
	blurredStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("240"))
	cursorStyle  = focusedStyle.Copy()
	noStyle      = lipgloss.NewStyle()
)

type PasswordModel struct {
	Pass       []textinput.Model
	focusIndex int
}

func InitializePasswordModel() (mod PasswordModel) {
	mod.Pass = make([]textinput.Model, 2)
	for i := range mod.Pass {
		t := textinput.New()
		t.CursorStyle = cursorStyle
		t.CharLimit = 64
		t.Placeholder = "Password"
		if i != 0 {
			t.Placeholder = "Re-enter password"
		}
		t.EchoMode = textinput.EchoPassword
		t.EchoCharacter = '*'
		mod.Pass[i] = t
	}
	// focus first field
	mod.Pass[0].Focus()
	mod.Pass[0].PromptStyle = focusedStyle
	mod.Pass[0].TextStyle = focusedStyle
	return
}

func (m PasswordModel) Init() tea.Cmd {
	return textinput.Blink
}

func (m PasswordModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "ctrl+c", "esc":
			// should exit without continuing?
			return m, tea.Quit
		case "enter", "down":
			if m.focusIndex == len(m.Pass)-1 {
				return m, tea.Quit
			} else {
				m.focusIndex++
			}

			cmds := make([]tea.Cmd, len(m.Pass))
			for i := 0; i <= len(m.Pass)-1; i++ {
				if i == m.focusIndex {
					// Set focused state
					cmds[i] = m.Pass[i].Focus()
					m.Pass[i].PromptStyle = focusedStyle
					m.Pass[i].TextStyle = focusedStyle
					continue
				}
				// Remove focused state
				m.Pass[i].Blur()
				m.Pass[i].PromptStyle = noStyle
				m.Pass[i].TextStyle = noStyle
			}

			return m, tea.Batch(cmds...)
		}
	}
	// Handle character input and blinking
	cmd := m.updateInputs(msg)

	return m, cmd
}

func (m *PasswordModel) updateInputs(msg tea.Msg) tea.Cmd {
	var cmds = make([]tea.Cmd, len(m.Pass))

	// Only text inputs with Focus() set will respond, so it's safe to simply
	// update all of them here without any further logic.
	for i := range m.Pass {
		m.Pass[i], cmds[i] = m.Pass[i].Update(msg)
	}

	return tea.Batch(cmds...)
}

func (m PasswordModel) View() string {
	buf := new(bytes.Buffer)
	for i := range m.Pass {
		fmt.Fprintf(buf, "%s", m.Pass[i].View())
		if i != len(m.Pass)-1 {
			fmt.Fprintln(buf)
		}
	}

	return buf.String()
}
