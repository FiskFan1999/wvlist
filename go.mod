module github.com/FiskFan1999/wvlist

go 1.18

require (
	github.com/gosimple/unidecode v1.0.1
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible
	github.com/thanhpk/randstr v1.0.4
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838
	golang.org/x/term v0.0.0-20210422114643-f5beecf764ed
)

require (
	github.com/atotto/clipboard v0.1.4 // indirect
	github.com/charmbracelet/bubbles v0.10.3 // indirect
	github.com/charmbracelet/bubbletea v0.20.0 // indirect
	github.com/charmbracelet/lipgloss v0.5.0 // indirect
	github.com/chromedp/cdproto v0.0.0-20220725225757-5988d9195a6c // indirect
	github.com/chromedp/chromedp v0.8.3 // indirect
	github.com/chromedp/sysutil v1.0.0 // indirect
	github.com/containerd/console v1.0.3 // indirect
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.1.0 // indirect
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/muesli/ansi v0.0.0-20211018074035-2e021307bc4b // indirect
	github.com/muesli/reflow v0.3.0 // indirect
	github.com/muesli/termenv v0.11.1-0.20220212125758-44cd13922739 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20220730100132-1609e554cd39 // indirect
)
