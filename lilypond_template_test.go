package main

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"testing"
	"text/template"
)

var (
	TestLilypondCase LilypondTemplateInput = LilypondTemplateInput{
		"2.20.0",
		"c'8 d' e' f' g' a' b' g' c''1",
	}
)

func TestLilypondTemplate(t *testing.T) {
	/*
		Test that the lilypond template
		can be properly used.
		And for a user input which
		clearly works, that it would be
		successful.
	*/
	if _, err := CheckForLilypondAtStart("lilypond"); err != nil {
		t.Logf("lilypond executable returned the following error (skipping lilypond test): %+v", err)
		t.Skip()
	}

	tmp, err := template.ParseFS(LilypondTemplateFS, LILYPOND_TEMPLATE_FILE)
	if err != nil {
		t.Fatalf("%+v", err)
	}

	var buf bytes.Buffer
	err = tmp.Execute(&buf, TestLilypondCase)
	if err != nil {
		t.Fatalf("%+v", err)
	}
	t.Log(buf.String())

	outFile, err := os.CreateTemp("", "*.pdf")
	if err != nil {
		t.Fatalf("%+v", err)
	}
	outFile.Close()
	defer os.Remove(outFile.Name())

	cmd := exec.Command("lilypond", "-dsafe", "-o", outFile.Name(), "-") // will read from stdin

	stdin, err := cmd.StdinPipe()
	if err != nil {
		t.Fatalf("%+v", err)
	}

	fmt.Fprintln(stdin, buf.String())
	stdin.Close()

	output, err := cmd.CombinedOutput()
	t.Log(string(output))
	if err != nil {
		t.Fatalf("%+v", err)
	}

}
